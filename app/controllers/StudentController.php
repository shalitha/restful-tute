<?php
class StudentController extends BaseController{
    // [/student] get method is redirected to this method by routes file
    public function index(){
        return Response::json(Student::all());
    }
    
    // [/student/$id] get method is redirected to this method by routes file
    public function show($id){
        return Response::json(Student::find($id));
    }
    // [/student] post method is redirected to this method by routes file
    public function store(){
        $student = new Student();
        $student->name = Input::json('name');
        $student->age = Input::json('age');
        $student->address = Input::json('address');
        return Response::json($student->save());
    }
    
    public function update($id){
        $edit = Student::find($id);
        $edit->name = Input::json('name');
        $edit->age = Input::json('age');
        $edit->address = Input::json('address');
        return Response::json($edit->save());
    }
    
    public function destroy($id){
        return Response::json(Student::find($id)->delete());
    }
}