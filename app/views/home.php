<html ng-app="restApp">
    <head>
        <title>REST Services</title>
        <link rel="stylesheet" href="css/bootstrap.min.css" />
        <script src="libs/angular.min.js"></script>
        <script>
            var app = angular.module("restApp", []);
            app.controller("MainCtrl", function($scope, $http){
        $scope.editing = false;
        $scope.getStudents = function(){
            var res = $http.get('/student');
            res.success(function(data){
                $scope.students = data;
            });
        };
        $scope.getStudents();
                
        $scope.editStudent = function(student){
            $scope.editing = true;
            $scope.editedStudent = student;
        };
                
        $scope.deleteStudent = function(id){
            var req = $http.delete('/student/'+id);
            req.success(function(data){
                if(data){
                    $scope.getStudents();
                }else{
                    alert("Not Saved");
                }
            });
            req.error(function(err){
                alert("Unable to send request");
                console.log(err);
            });
        };
                
        $scope.saveStudent = function(){
            //console.log($scope.newStudent);
            var req = $http.post('/student', $scope.newStudent);
            req.success(function(data){
                if(data){
                    $scope.getStudents();
                    alert("Saved");
                }else{
                    alert("Not Saved");
                }
            });
            req.error(function(err){
                alert("Unable to send request");
                console.log(err);
            });
            
        };
                
        $scope.updateStudent = function(){
            var req = $http.put('/student/'+$scope.editedStudent.id,          
                     $scope.editedStudent);
            req.success(function(data){
                if(data){
                    $scope.getStudents();
                    alert("Saved");
                }else{
                    alert("Not Saved");
                }
            });
            req.error(function(err){
                alert("Unable to send request");
                console.log(err);
            });
        };
    });
            
        </script>
    </head>
    <body>
        <div class="container" ng-controller="MainCtrl">
            <div class="data col-lg-6">
    <table>
        <tr>
            <th>ID</th>
            <th>Name</th>
            <th>Age</th>
            <th>&nbsp;</th>
            <th>&nbsp;</th>
        </tr>
        <tr ng-repeat="student in students">
            <td>{{student.id}}</td>
            <td>{{student.name}}</td>
            <td>{{student.age}}</td>
            <td><a href="" ng-click="editStudent(student)">Edit</a></td>
            <td><a href="" ng-click="deleteStudent(student.id)">Delete</a></td>
        </tr>
    </table>
            </div>
            <div class="form col-lg-6">
                <div class="add_student" ng-show="!editing">
            <h1>Creating a student</h1>
			<form class="form-horizontal" ng-submit="saveStudent()">
				<div class="row">
					<label class="control-label">Name : </label>
					<input class="form-control" ng-model="newStudent.name" />
				</div>
				<div class="row">
					<label class="control-label">Age : </label>
					<input class="form-control" ng-model="newStudent.age" />
				</div>
				<div class="row">
					<label class="control-label">Address : </label>
					<input class="form-control" ng-model="newStudent.address" />
				</div>
				<div class="row">
					<input type="submit" class="form-control" value="Save" />
				</div>
			</form>
                </div>
                <div class="edit_student" ng-show="editing">
        <h1>Updating a student</h1>
        <form class="form-horizontal" ng-submit="updateStudent()">
            <div class="row">
                <label class="control-label">Name : </label>
                <input class="form-control" ng-model="editedStudent.name" />
            </div>
            <div class="row">
                <label class="control-label">Age : </label>
                <input class="form-control" ng-model="editedStudent.age" />
            </div>
            <div class="row">
                <label class="control-label">Address : </label>
                <input class="form-control" ng-model="editedStudent.address" />
            </div>
            <div class="row">
                <input type="submit" class="form-control" value="Save" />
            </div>
        </form>
                </div>
            </div> 
        </div>
    </body>
</html>